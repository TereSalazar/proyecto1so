package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "github.com/mackerelio/go-osstat/memory"
    "encoding/json"
)

type cadJson struct{
	PORCENTAJE float64 `json:"PORCENTAJE"`
}

func main() {

    http.HandleFunc("/memoria", func(w http.ResponseWriter, r *http.Request) {
       
       memory, err := memory.Get()
    	if err !=nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
       		return
    	}
	
        memoriaP:= float64(memory.Used)/float64(memory.Total)
               
        dato := cadJson{
        	PORCENTAJE: memoriaP,
        }
       
		w.Header().Set("Content-Type","application/json")
		json.NewEncoder(w).Encode(dato)
        
	})


    log.Fatal(http.ListenAndServe(":8081", nil))
    
}

